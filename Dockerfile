FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/data
WORKDIR /app/code

ARG VERSION=1.3.9

# install cloud-torrent
RUN curl --fail -# -L https://github.com/boypt/simple-torrent/releases/download/$VERSION/cloud-torrent_linux_amd64.gz \
	| gzip -d - > /usr/local/bin/cloud-torrent && \
	chmod +x /usr/local/bin/cloud-torrent

COPY start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
