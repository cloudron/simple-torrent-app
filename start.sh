#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

echo "=> Ensure config file"
# https://github.com/boypt/simple-torrent/wiki/Config-File
# for some reason it needs an empty file to exist only to put the default values in,
# most likely to determine which of the various formats are wanted
touch /app/data/cloud-torrent.yaml

echo "=> Starting cloud torrent"
cd /app/data
exec /usr/local/bin/gosu cloudron:cloudron /usr/local/bin/cloud-torrent
