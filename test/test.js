#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TIMEOUT = 100000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    const TORRENT_URL_INPUT_XPATH = '/html/body/div/section[2]/div[2]/input';
    const TORRENT_URL = 'magnet:?xt=urn:btih:2bd9d334e8d1e5bd7768755173222db5c6dea13b&dn=archlinux-2021.07.01-x86_64.iso';
    const TORRENT_FILE_NAME = 'archlinux-2021.07.01-x86_64.iso';

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await browser.wait(until.elementLocated(By.xpath(TORRENT_URL_INPUT_XPATH)), TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
    }

    async function loadTorrent() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath(TORRENT_URL_INPUT_XPATH)), TIMEOUT);
        await browser.findElement(By.xpath(TORRENT_URL_INPUT_XPATH)).sendKeys(TORRENT_URL);
        // give the input validator some time
        await browser.sleep(2000);
        await browser.findElement(By.xpath(TORRENT_URL_INPUT_XPATH)).sendKeys(Key.ENTER);
        // give the angular app some time to load the torrent
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath(`//*[text()="${TORRENT_FILE_NAME}"]`)), TIMEOUT);
    }

    async function isDownloading() {
        await browser.get(`https://${app.fqdn}`);
        // give the angular app some time to load the torrent
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//h5[@ng-click="section_expanded_toggle()"]')), TIMEOUT);
        await browser.findElement(By.xpath('//h5[@ng-click="section_expanded_toggle()"]')).click();
        await browser.wait(until.elementLocated(By.xpath(`//*[text()="${TORRENT_FILE_NAME}"]`)), TIMEOUT);
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can load torrent', loadTorrent);
    it('can logout', logout);
    it('can restart app', function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });
    it('can login', login);
    it('still downloading', isDownloading);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('still downloading', isDownloading);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login);
    it('still downloading', isDownloading);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id com.github.cloudtorrent --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can load torrent', loadTorrent);
    it('can logout', logout);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id} --no-backup`, EXEC_ARGS);
    });

    it('can login', login);
    it('is still downloading', isDownloading);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
