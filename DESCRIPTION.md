### Overview

Simple Torrent (was Cloud Torrent) is a remote torrent client, written in Go (golang). It starts torrents on the server, which are downloaded as sets of files on the local disk of the server, which are then retrievable or streamable via HTTP.

### Features

* Single binary
* Cross platform
* Embedded torrent search
* Real-time updates
* Mobile-friendly
